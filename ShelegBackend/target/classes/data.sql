INSERT INTO convoys
(id, leader_id, rear_id, mission_type, start_date, finish_date, cargo_name,
start_place, start_coord, end_place, end_coord, commander, commander_phone, is_arrived, is_in_motion)
VALUES(
1,
'77-544-43',
'22-445-34',
'הובלה',
'2022-01-31 23:07:00',
'2022-02-01 02:31:00',
'מזון',
'מבשרת ציון',
'31.8041/35.1578',
'רמת גן',
'32.0703/34.8247',
'אור מאיר',
'0544444444',
false,
false
);

INSERT INTO convoys
(id, leader_id, rear_id, mission_type, start_date, finish_date, cargo_name,
start_place, start_coord, end_place, end_coord, commander, commander_phone, is_arrived, is_in_motion)
VALUES(
2,
'66-000-66',
'88-888-88',
'הפממה',
'2022-01-23 22:22:35',
'2022-01-24 01:56:12',
'ציוד טקטי',
'חיפה',
'32.7993/34.9856',
'מבשרת ציון',
'31.8041/35.1578',
'אלמוג בגטי',
'0522222222',
false,
false
);

INSERT INTO convoys
(id, leader_id, rear_id, mission_type, start_date, finish_date, cargo_name,
start_place, start_coord, end_place, end_coord, commander, commander_phone, is_arrived, is_in_motion)
VALUES(
3,
'36-982-10',
'24-005-98',
'הפממה',
'2021-12-16 10:18:27',
'2021-12-16 21:06:02',
'בגדים',
'גבעתיים',
'32.0709/34.8085',
'באר שבע',
'31.2613/34.8089',
'תמר פדן',
'0588888888',
false,
false
);

INSERT INTO convoys
(id, leader_id, rear_id, mission_type, start_date, finish_date, cargo_name,
start_place, start_coord, end_place, end_coord, commander, commander_phone, is_arrived, is_in_motion)
VALUES(
4,
'42-269-00',
'12-452-94',
'הובלה',
'2021-12-02 13:38:52',
'2021-12-02 19:18:47',
'מזון',
'כוכב יאיר',
'32.2095/34.9892',
'אילת',
'29.5664/34.9500',
'רמי סלבדור',
'0588888888',
false,
false
);



INSERT INTO convoy_properties (name, display_name) VALUES ('id', 'מספר שדרה');
INSERT INTO convoy_properties (name, display_name) VALUES ('leaderId', 'מספר רכב בראש');
INSERT INTO convoy_properties (name, display_name) VALUES ('rearId', 'מספר רכב בזנב');
INSERT INTO convoy_properties (name, display_name) VALUES ('missionType', 'סוג משימה');
INSERT INTO convoy_properties (name, display_name) VALUES ('startDate', 'זמן תחילת תנועה');
INSERT INTO convoy_properties (name, display_name) VALUES ('finishDate', 'זמן מתוכנן לסיום');
INSERT INTO convoy_properties (name, display_name) VALUES ('startPlace', 'מקום מוצא');
INSERT INTO convoy_properties (name, display_name) VALUES ('startCoord', 'קורדינטת מוצא');
INSERT INTO convoy_properties (name, display_name) VALUES ('endPlace', 'מקום יעד');
INSERT INTO convoy_properties (name, display_name) VALUES ('endCoord', 'קורדינטת יעד');
INSERT INTO convoy_properties (name, display_name) VALUES ('commander', 'מפקד שדרה');
INSERT INTO convoy_properties (name, display_name) VALUES ('commanderPhone', 'מספר ליצירת קשר');
