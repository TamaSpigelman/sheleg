package ShelegBackend.Controllers;

import ShelegBackend.Models.Convoy;
import ShelegBackend.Models.ConvoyProperty;
import ShelegBackend.Services.ConvoyPropertiesService;
import ShelegBackend.Services.ConvoysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.rmi.NoSuchObjectException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/convoys")
public class ConvoysController {

    @Autowired
    private ConvoysService convoysService;
    @Autowired
    private ConvoyPropertiesService convoyPropertiesService;

    @GetMapping("")
    public List<Convoy> getAll() {
        return this.convoysService.getAll();
    }

    @GetMapping("/{convoyId}")
    public Convoy findById(@PathVariable int convoyId) throws NoSuchObjectException {
        return this.convoysService.getConvoy(convoyId);
    }

    @GetMapping("/properties")
    public List<ConvoyProperty> getAllProperties() {
        return this.convoyPropertiesService.getAll();
    }

    @PutMapping("")
    public Convoy updateConvoy(@RequestBody Convoy convoy) throws NoSuchObjectException {
        return this.convoysService.updateConvoy(convoy);
    }

    @PatchMapping("/{convoyId}/in-motion")
    public Convoy updateConvoyInMotion(@PathVariable int convoyId) throws NoSuchObjectException {
        return this.convoysService.updateConvoyInMotion(convoyId);
    }

    @PatchMapping("/{convoyId}/arrived")
    public Convoy updateConvoyArrived(@PathVariable int convoyId) throws NoSuchObjectException {
        return this.convoysService.updateConvoyArrived(convoyId);
    }
}
