package ShelegBackend.Repositories;

import ShelegBackend.Models.Convoy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConvoysRepository extends JpaRepository<Convoy, Integer> {}
