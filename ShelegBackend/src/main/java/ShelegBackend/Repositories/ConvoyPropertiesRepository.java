package ShelegBackend.Repositories;

import ShelegBackend.Models.ConvoyProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConvoyPropertiesRepository extends JpaRepository<ConvoyProperty, String> {}
