package ShelegBackend.Services;

import ShelegBackend.Models.ConvoyProperty;
import ShelegBackend.Repositories.ConvoyPropertiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConvoyPropertiesService {

    @Autowired
    private ConvoyPropertiesRepository convoyPropertiesRepository;

    public List<ConvoyProperty> getAll() {
        return this.convoyPropertiesRepository.findAll();
    }
}
