package ShelegBackend.Services;

import ShelegBackend.Models.Convoy;
import ShelegBackend.Repositories.ConvoysRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.rmi.NoSuchObjectException;
import java.util.List;

@Service
public class ConvoysService {

    @Autowired
    private ConvoysRepository convoysRepository;

    public List<Convoy> getAll() {
        return this.convoysRepository.findAll();
    }

    public Convoy updateConvoy(Convoy updatedConvoy) throws NoSuchObjectException {
        this.convoysRepository
                .findById(updatedConvoy.id())
                .orElseThrow(() ->
                        new NoSuchObjectException("Failed updating the convoy: " +
                                "No convoy was found for id " + updatedConvoy.id()));

        return this.convoysRepository.save(updatedConvoy);
    }

    public Convoy updateConvoyInMotion(int convoyId) throws NoSuchObjectException {
        Convoy convoy = this.getConvoy(convoyId);
        convoy.updateInMotion();

        return this.convoysRepository.save(convoy);
    }

    public Convoy updateConvoyArrived(int convoyId) throws NoSuchObjectException {
        Convoy convoy = this.getConvoy(convoyId);
        convoy.updateIsArrived();

        return this.convoysRepository.save(convoy);
    }

    public Convoy getConvoy(int id) throws NoSuchObjectException {
        return this.convoysRepository
                .findById(id)
                .orElseThrow(() ->
                        new NoSuchObjectException("No convoy was found for id " + id));
    }
}
