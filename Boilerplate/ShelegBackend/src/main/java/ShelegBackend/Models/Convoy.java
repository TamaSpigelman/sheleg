package ShelegBackend.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "convoys")
public class Convoy {

    @Id
    @JsonProperty
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JsonProperty
    @Column(name = "leader_id")
    private String leaderId;

    @JsonProperty
    @Column(name = "rear_id")
    private String rearId;

    @JsonProperty
    @Column(name = "mission_type")
    private String missionType;

    @JsonProperty
    @Column(name = "start_date")
    private Date startDate;

    @JsonProperty
    @Column(name = "finish_date")
    private Date finishDate;

    @JsonProperty
    @Column(name = "cargo_name")
    private String cargoName;

    @JsonProperty
    @Column(name = "start_place")
    private String startPlace;

    @JsonProperty
    @Column(name = "start_coord")
    private String startCoord;

    @JsonProperty
    @Column(name = "end_place")
    private String endPlace;

    @JsonProperty
    @Column(name = "end_coord")
    private String endCoord;

    @JsonProperty
    @Column(name = "commander")
    private String commander;

    @JsonProperty
    @Column(name = "commander_phone")
    private String commanderPhone;

    @JsonProperty
    @Column(name = "is_arrived")
    private boolean isArrived;

    @JsonProperty
    @Column(name = "is_in_motion")
    private boolean inMotion;

    public int id() {
        return this.id;
    }

    public void updateInMotion() {
        this.isArrived = false;
        this.inMotion = true;
    }

    public void updateIsArrived() {
        this.isArrived = true;
        this.inMotion = false;
    }
}
