package ShelegBackend.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
@Entity
@Table(name="convoy_properties")
public class ConvoyProperty {

    @Id
    @JsonProperty
    @Column(name="name")
    private String name;

    @JsonProperty
    @Column(name="display_name")
    private String displayName;
}
