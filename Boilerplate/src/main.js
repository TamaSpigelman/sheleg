import Vue from "vue";
import App from "./App.vue";
import "bulma/css/bulma.css";
import "bootstrap/dist/css/bootstrap.min.css";
// import "bootstrap/dist/css/bootstrap.css";
import SmartTable from 'vuejs-smart-table';
Vue.use(SmartTable)


Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
