import axiosInstance from "./axiosInstance";

export default {
  convoys() {
    return {
      getAll: () => axiosInstance.get(`convoys`),
      findById: (id) => axiosInstance.get(`convoys/${id}`),
      getProps: () => axiosInstance.get(`convoys/properties`),
      reportConvoyInMotion: (convoyId) =>
        axiosInstance.patch(`convoys/${convoyId}/in-motion`),
      reportConvoyArrived: (convoyId) =>
        axiosInstance.patch(`convoys/${convoyId}/arrived`),
      updateConvoy: (convoy) => axiosInstance.put(`convoys`, convoy),
    };
  },
};
